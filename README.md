# yii2-module/yii2-export

A module that exports data from a database.

![coverage](https://gitlab.com/yii2-module/yii2-export/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/yii2-module/yii2-export/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar install yii2-module/yii2-export ^8`


## Configuration

This module needs the following components to be set at the configuration level:

- 'db_export' should be a `\yii\db\Connection`

If you already have a database connection, you may use the following trick :

`'db_export' => function() { return \Yii::$app->get('db'); },`

where 'db' is the id of your database connection.

This module uses the following parameters to be set at the configuration level:

- NONE


Then the module should be configured as follows (in `console.php` or `web.php`) :

```php
$config = [
	...
	'modules' => [
		...
		'export' => [
			'class' => 'Yii2Module\Yii2Export\ExportModule',
		],
		...
	],
	...
];
```


## License

MIT (See [license file](LICENSE))
