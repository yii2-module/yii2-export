/**
 * Database structure required by Yii2Module\Yii2Information\InformationModule
 * 
 * @author Anastaszor
 * @link https://gitlab.com/yii2-module/yii2-information
 * @license MIT
 */

SET foreign_key_checks = 0;

DROP TABLE IF EXISTS `export_reminder`;

SET foreign_key_checks = 1;

CREATE TABLE `export_reminder`
(
	`export_reminder_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'The id of the reminder',
	`module_id` VARCHAR(128) NOT NULL COLLATE ascii_bin COMMENT 'The module that registered this object',
	`format_id` VARCHAR(128) NOT NULL COLLATE ascii_bin COMMENT 'The format that was used for this object',
	`object_class` VARCHAR(128) NOT NULL COLLATE ascii_bin COMMENT 'The object class that was registered',
	`object_last_id` VARCHAR(128) NOT NULL COLLATE ascii_bin COMMENT 'The last id of the object that was exported',
	`object_count` INT(11) NOT NULL COMMENT 'The number of objects that were exported',
	UNIQUE INDEX `export_reminder_module_class` (`module_id`, `object_class`)
)
ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci
COMMENT='The table to store the reminders of the already exported data';
