<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-export library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Export;

use yii\base\Module;
use yii\BaseYii;
use Yii2Extended\Metadata\Bundle;
use Yii2Extended\Metadata\Record;
use Yii2Module\Helper\BootstrappedModule;
use Yii2Module\Yii2Export\Models\ExportReminder;

/**
 * ExportModule class file.
 *
 * This module is made to export informations to file from any data source.
 *
 * @author Anastaszor
 */
class ExportModule extends BootstrappedModule
{
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getBootstrapIconName()
	 */
	public function getBootstrapIconName() : string
	{
		return 'box-arrow-right';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getLabel()
	 */
	public function getLabel() : string
	{
		return BaseYii::t('ExportModule.Module', 'Export');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getEnabledBundles()
	 */
	public function getEnabledBundles() : array
	{
		return [
			'export' => new Bundle(BaseYii::t('ExportModule.Module', 'Export'), [
				'export' => (new Record(ExportReminder::class, 'export', BaseYii::t('ExportModule.Module', 'Export')))->enableFullAccess(),
			]),
		];
	}
	
}
